﻿using System;

namespace Basic.netMentor
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Fechas fechasEj = new Fechas();
            fechasEj.InstanciaDateTime();
            fechasEj.LocalizacionCultureInfo();
            fechasEj.PropertiesDateTime();
            fechasEj.MethodsDateitme();
            fechasEj.DateComparison();
            fechasEj.PrintWithFormattedDate();

            //DateTime dtError = new DateTime(2015, 12, 32); //throws exception: day out of range  //esto es otro tema: StackTrace Class 
            //https://learn.microsoft.com/en-us/dotnet/api/system.diagnostics.stacktrace?view=net-6.0
            //https://learn.microsoft.com/en-us/dotnet/api/system.exception.stacktrace?view=net-6.0


        }
    }
}
