﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Basic.netMentor
{
    /*  Colocar el cursor sobre la línea y pulsar F9
        Boton secundario sobre la linea > Breakpoint > insertar breakpoint
        En el lateral del fichero pulsar con el botón izquierdo del ratón, y nos añadira un 
            breakpoint
     */
    /*  F5 continua hasta el siguiente breakpoint
        F10 Ejecuta esa línea de código
        F11 Ejecuta la siguiente sentencia de código, esto quiere decir que si por ejemplo 
            estamos llamando a un método dentro de esa linea de codigo, pulsando F11 entraria 
            dentro del método.
     */

    /*  Disponemos de acceso al Stack de la llamada, que incluye cada punto por el que el 
     *  proceso ha pasado:

        call stack

        Si hacemos doble click en cualquiera de ellos, Visual studio nos llevar a ese punto, 
        pero no solo eso, las variables tendrán el valor que tenían en ese punto. Lo cual 
        facilita mucho a la hora de comprobar errores. 
     */
    /*  Como hemos dicho cuando estamos debugueando tenemos acceso a todas las variables 
     *  disponibles en el scope, por lo que podemos consultar su valor o incluso operar con 
     *  ellas en la caja "watch" de visual stuido, lo cual facilita mucho cuando tenemos grandes 
     *  bloques de informaición como listas de empleados etc. 
     */
    class ModoDebug
    {
        /*
         * 1. Entrar por teclado 2 numeros y mostrar por pantalla todos los pares
         * 2. Escribir un programa que imprima los numeros perfectos entre 1 y n
         *    (un numero perfecto es cuando un numero es igual a la suma de sus divisores. Ej. 1+2+3=6)
         * 3. Preguntar por teclado por un nombre y repetir hasta que introduzca "NetMentor"
         * 4. Los 3 ejercicios anteriores tienen que estar Dentro de un menu; y entonces realizar las
         *    acciones necesarias.
         *    (Notas: Recuerda hacer una opcion para salir del programa, utilizar funciones)
         */
    }
}
