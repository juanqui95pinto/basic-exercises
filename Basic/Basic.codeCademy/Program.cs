﻿using System;

namespace Basic.codeCademy
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello CODE-CADEMY Cheatsheets!");
            DataTypes_Variables dtv = new DataTypes_Variables();
          //  dtv.VariablesAndTypes();

            LogicAndConditionals lac = new LogicAndConditionals();
          //  lac.Logic();
          //  lac.Conditional();

            Methods methods = new Methods();
            /// methods.ConceptOfMethod();
            // methods.MoreConcepts();

            ArraysAndLoops aal = new ArraysAndLoops();
            //aal.Arrays();
            //aal.Loops();

            ClassesAndObjects aso = new ClassesAndObjects();
            //aso.Clases();

            //Interfaces and Inheritance 

            //References
            References r = new References();
            //r.ComplementCode();

            //List and Linq
            ListAndLinq lal = new ListAndLinq();
            // lal.List();
            lal.Linq();


        }
    }
}
