﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Basic.codeCademy
{
    public class DataTypes_Variables
    {
        public void VariablesAndTypes()
        {
            string foo = "Hello";
            string bar = "How are you?";
            int x = 5;

            Console.WriteLine(foo);


            //Arithmetic Operators
            int result;

            result = 10 + 5;  // 15
            Console.WriteLine(result);

            result = 10 - 5;  // 5
            Console.WriteLine(result);

            result = 10 * 5;  // 50
            Console.WriteLine(result);

            result = 10 / 5;  // 2
            Console.WriteLine(result);

            result = 10 % 5;  // 0
            Console.WriteLine(result);

            //Unary Operator
            int a = 10;
            a++;
            Console.WriteLine(a);

            //some Methods
            double x2 = 81;
            Console.Write(Math.Sqrt(x2));

            double pow_ab = Math.Pow(6, 2);
            Console.WriteLine("\n"+pow_ab);

            string str2 = "This is C# Program xsdd_$#%";
            // string converted to Upper case 
            string upperstr2 = str2.ToUpper();
            Console.WriteLine(upperstr2);

            //IndexOf() in C#
            /* In C#, the IndexOf() method is a string method used to find 
             * the index position of a specified character in a string. 
             * The method returns -1 if the character isn’t found.
             */
            string str = "Divyesh";

            // Finding the index of character  
            // which is present in string and 
            // this will show the value 5 
            int index1 = str.IndexOf('s');

            Console.WriteLine("-Divyesh- The Index Value of character 's' is " + index1);
            //The Index Value of character 's' is 5

            /* Bracket Notation
             * Strings contain characters. One way these char values can be accessed is 
             * with bracket notation. We can even store these chars in separate variables.
             *
             * We access a specific character by using the square brackets on the string, 
             * putting the index position of the desired character between the brackets. 
             * For example, to get the first character, you can specify variable[0]. 
             * To get the last character, you can subtract one from the length of the string.
             */
            // Get values from this string.
            string value = "Dot Net Perls";

            //variable first contains letter D 
            char first = value[0];

            //Second contains letter o
            char second = value[1];

            //last contains letter s
            char last = value[value.Length - 1];
            Console.WriteLine("-Dot Net Perls-     first: {0}, second: {1}, last: {2} ", first, second, last);


            // Substring() in C#
            /* In C#, Substring() is a string method used to retrieve part of a string 
             * while keeping the original data intact. The substring that you retrieve 
             * can be stored in a variable for use elsewhere in your program.
             */
            string myString = "Divyesh";
            string test1 = myString.Substring(3);
            Console.WriteLine(myString+ " Substring(3)-> "+ myString.Substring(3));

            //String Concatenation in C#
            // Declare strings    
            string firstName = "Divyesh";
            string lastName = "Goardnan";

            // Concatenate two string variables    
            string name = firstName + " " + lastName;
            Console.WriteLine(name);
            //Ths code will output Divyesh Goardnan


            //.ToLower() in C#
            /* In C#, .ToLower() is a string method that converts every character to lowercase. 
             * If a character does not have a lowercase equivalent, it remains unchanged. 
             * For example, special symbols remain unchanged.
             */
            string mixedCase = "This is a MIXED case string.";

            // Call ToLower instance method, which returns a new copy.
            string lower = mixedCase.ToLower();
            Console.WriteLine(lower);
            //variable lower contains "this is a mixed case string."


            //String Length in C#
            //The string class has a Length property, which returns the number of characters in the string.
            string a2 = "One example";
            Console.WriteLine("LENGTH: " + a2.Length);
            // This code outputs 11


            //String Interpolation in C#
            /* String interpolation provides a more readable and convenient syntax to create formatted 
             * strings. It allows us to insert variable values and expressions in the middle of a string 
             * so that we don’t have to worry about punctuation or spaces.
             */
            int id = 100;

            // We can use an expression with a string interpolation.
            string multipliedNumber = $"The multiplied ID is {id * 10}.";

            Console.WriteLine(multipliedNumber);
            // This code would output "The multiplied ID is 1000."
            Console.WriteLine();


            //Console.ReadLine()
            /* The Console.ReadLine() method is used to get user input. The user input can be 
             * stored in a variable. This method can also be used to prompt the user to 
             * press enter on the keyboard.
             */
            Console.WriteLine("Enter your name: ");

            name = Console.ReadLine();
            Console.WriteLine(name);


            //String New-Line "\n", 

            // This is a single line comment
            /* This is a multi-line comment
               and continues until the end
               of comment symbol is reached */

            //Console.WriteLine()
            /* The Console.WriteLine() method is used to print text to the console. 
             * It can also be used to print other data types and values stored in variables.
             */
            Console.WriteLine("Hello, world!");
            // Prints: Hello, world!
        }
    }
}
